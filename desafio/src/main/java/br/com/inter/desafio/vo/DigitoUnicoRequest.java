package br.com.inter.desafio.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DigitoUnicoRequest {
	
	private String numero;
	
	private Integer repeticoes;
	
	private String usuarioId;
}
