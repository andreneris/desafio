package br.com.inter.desafio.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.inter.desafio.model.DigitoUnico;

public interface DigitoUnicoRepository extends MongoRepository<DigitoUnico, String> {

}
