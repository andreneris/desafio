package br.com.inter.desafio.vo;

import java.util.List;

import br.com.inter.desafio.model.DigitoUnico;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsuarioRequest {
	private String nome;
	
	private String email;
	
	private List<DigitoUnico> digitoUnico;
}
