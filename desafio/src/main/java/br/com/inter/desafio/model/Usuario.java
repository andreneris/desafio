package br.com.inter.desafio.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Usuario {
	private String id;
	
	private String nome;
	
	private String email;
	
	private List<DigitoUnico> digitoUnico;
}
