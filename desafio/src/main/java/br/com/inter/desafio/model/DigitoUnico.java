package br.com.inter.desafio.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class DigitoUnico {

	private String numero;
	
	private Integer repeticoes;
	
	private Integer digitoUnico;
	
	private String usuarioId;
}
