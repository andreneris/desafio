package br.com.inter.desafio.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.desafio.model.DigitoUnico;
import br.com.inter.desafio.model.Usuario;
import br.com.inter.desafio.repository.DigitoUnicoRepository;
import br.com.inter.desafio.repository.UsuarioRepository;
import br.com.inter.desafio.vo.UsuarioRequest;

@RestController
@RequestMapping("desafio/usuario")
public class UsuarioController {
	@Autowired
	UsuarioRepository desafioRepository;
	
	@Autowired
	DigitoUnicoRepository digitoUnicoRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Usuario>> listar(){
		List<Usuario> usuarios = desafioRepository.findAll();
		
		for (Usuario usuario:usuarios) {
			DigitoUnico digitoUnico = new DigitoUnico();
			digitoUnico.setUsuarioId(usuario.getId());
			Example<DigitoUnico> digitoUnicoExample = Example.of(digitoUnico);
			
			List<DigitoUnico> digitoUnicoList = digitoUnicoRepository.findAll(digitoUnicoExample);
			usuario.setDigitoUnico(digitoUnicoList);
			
		}
		
		
		return new ResponseEntity<>(usuarios,HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> criar(@RequestBody @Valid UsuarioRequest request){
		Usuario usuario = new Usuario();
		usuario.setNome(request.getNome());
		usuario.setEmail(request.getEmail());
		desafioRepository.save(usuario);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@RequestMapping(path="/{id}",method=RequestMethod.PUT)
	public ResponseEntity<Void> alterar (
			@PathVariable String id,
			@RequestBody @Valid UsuarioRequest request){
		Optional<Usuario> usuario = desafioRepository.findById(id);
		
		if (usuario.isPresent()) {
			usuario.get().setNome(request.getNome());
			usuario.get().setEmail(request.getEmail());
			desafioRepository.save(usuario.get());
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}		
	}
	
	@RequestMapping(path="/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Void> excluir (
			@PathVariable String id){
		Optional<Usuario> usuario = desafioRepository.findById(id);
		
		if (usuario.isPresent()) {
			desafioRepository.delete(usuario.get());
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}		
	}	
}
