package br.com.inter.desafio.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.desafio.model.DigitoUnico;
import br.com.inter.desafio.repository.DigitoUnicoRepository;
import br.com.inter.desafio.service.DigitoUnicoService;
import br.com.inter.desafio.vo.DigitoUnicoRequest;



@RestController
@RequestMapping("desafio/digitounico")
public class DigitoUnicoController {
	@Autowired
	private DigitoUnicoService digitoUnicoService;
	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;

	@RequestMapping( method=RequestMethod.POST)
	public ResponseEntity<Integer> criar (
			@RequestBody @Valid DigitoUnicoRequest request) {
		
		Integer digito = digitoUnicoService.digito_unico(request.getNumero(), request.getRepeticoes());
		
		DigitoUnico digitoUnico = new DigitoUnico();
		digitoUnico.setNumero(request.getNumero());
		digitoUnico.setRepeticoes(request.getRepeticoes());
		digitoUnico.setDigitoUnico(digito);
		if (request.getUsuarioId()!=null) {			
			digitoUnico.setUsuarioId(request.getUsuarioId());
		}
		
		digitoUnicoRepository.save(digitoUnico);
		return new ResponseEntity<>(digito,HttpStatus.CREATED);
	}
	
	@RequestMapping(path="{userId}", method=RequestMethod.GET)
	public ResponseEntity<List<DigitoUnico>> listar(@PathVariable String userId){
		
		DigitoUnico digitoUnico = new DigitoUnico();
		digitoUnico.setUsuarioId(userId);
		Example<DigitoUnico> digitoUnicoExample = Example.of(digitoUnico);		
		
		List<DigitoUnico> digitoUnicoList = digitoUnicoRepository.findAll(digitoUnicoExample);
		return new ResponseEntity<>(digitoUnicoList,HttpStatus.OK);
	}
}
