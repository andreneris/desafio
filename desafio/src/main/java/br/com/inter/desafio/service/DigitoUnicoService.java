package br.com.inter.desafio.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class DigitoUnicoService {
private Map<String, Integer> cacheMap = new HashMap<>();
	
	public Integer digito_unico(String numero, int repeticoes){
		Integer result=0;
		String digitos="";
		for (int i=0; i<repeticoes;i++) {
			digitos +=numero;
		}
		
		if (!this.cacheMap.containsKey(digitos)) {

			for (int i=0;i<digitos.length();i++) {
				result = result + Integer.parseInt(digitos.substring(i,i+1));
			}
			
			if (this.cacheMap.size()<11) {		
				this.cacheMap.put(digitos,result);
			}
			
		} else {
			result = this.cacheMap.get(digitos);
		}
				
		return result;
	}
}
