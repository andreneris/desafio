package br.com.inter.desafio.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.inter.desafio.model.Usuario;

public interface UsuarioRepository extends MongoRepository<Usuario, String> {

}
