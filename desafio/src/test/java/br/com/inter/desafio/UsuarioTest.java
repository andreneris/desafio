package br.com.inter.desafio;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.inter.desafio.controller.UsuarioController;
import br.com.inter.desafio.model.Usuario;
import br.com.inter.desafio.vo.UsuarioRequest;

@RunWith(SpringRunner.class)
@SpringBootTest

public class UsuarioTest {
	
	@Autowired
	UsuarioController usuarioController;
	
	@Test
    public void contexLoads() throws Exception {
		/*Test criar usuario*/
		UsuarioRequest usuarioRequest  = new UsuarioRequest("Jose","jose@gmail.com",null);		
		ResponseEntity<Void> responseCriar = usuarioController.criar(usuarioRequest);
		assertThat(responseCriar.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		
		/*Test listar*/
		ResponseEntity<List<Usuario>> responseListar = usuarioController.listar();
		assertThat(responseListar.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseListar.getBody().size()).isGreaterThan(0);
        
        /*Test alterar*/
        UsuarioRequest usuarioAlterarRequest  = new UsuarioRequest("Antonio","antonio@gmail.com",null);
        String userId = responseListar.getBody().get(0).getId();        
        ResponseEntity<Void> responseAlterar = usuarioController.alterar(userId,usuarioAlterarRequest);
		assertThat(responseAlterar.getStatusCode()).isEqualTo(HttpStatus.OK);
		        
        ResponseEntity<Void> responseExcluir = usuarioController.excluir(userId);
		assertThat(responseExcluir.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

}
