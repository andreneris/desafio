package br.com.inter.desafio;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
//import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import br.com.inter.desafio.controller.DigitoUnicoController;
import br.com.inter.desafio.model.DigitoUnico;
import br.com.inter.desafio.vo.DigitoUnicoRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DigitoUnicoTest {
	@Autowired
	DigitoUnicoController digitoUnicoController;
	
	@Test
    public void contexLoads() throws Exception {
		/*Test criar digito unico*/
		DigitoUnicoRequest request = new DigitoUnicoRequest("9875",1,null);
		ResponseEntity<Integer> responseCriar = digitoUnicoController.criar(request); 
        assertThat(responseCriar.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(responseCriar.getBody()).isEqualTo(29);
        
        /*Test listar digto unico*/
        ResponseEntity<List<DigitoUnico>> responseListar = digitoUnicoController.listar(null); 
        assertThat(responseListar.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseListar.getBody().size()).isGreaterThan(1);
        
    }

}
