# desafio

# Script de Test

mvn --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true verify

# Script de Deploy

mvn --batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true deploy


# Pipeline

Configurado arquivo gitlab-ci.yml para test e deploy

https://gitlab.com/andreneris/desafio/-/packages/